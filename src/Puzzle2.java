import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Puzzle2 {



    /** Solve the word puzzle.
     * @param args three words (addend1 addend2 sum)
     */


    public static void main(String[] args) {
        solvePuzzle("SEND","MORE","MONEY");
        solvePuzzle("YKS","KAKS","KOLM");
        solvePuzzle("A","B","C");
    }


    private static GenericStack<Integer> numbersStack = new GenericStack<>();
    private static GenericStack<String> lettersStack = new GenericStack<>();
    private static int combinationsCount = 0;
    private static int solutionCount = 0;
    private static Set<String> letterSet = new LinkedHashSet<>();
    private static String solutionExample;
    private static String[] input11;
    private static String[] input22;
    private static String[] input33;

    private static void solvePuzzle(String input1, String input2, String input3){

        String inputCharsString = input1 + input2 + input3;
        String[] inputCharsArray = inputCharsString.split("");

        input11 = input1.split("");
        input22 = input2.split("");
        input33 = input3.split("");

        // GET UNIQUE LETTERS FROM INPUT STRINGS
        letterSet.addAll(Arrays.asList(inputCharsArray));

        int set = 9;
        List<Integer> numbers = IntStream.rangeClosed(0, set).boxed().collect(Collectors.toList());
        //recursiveTree(numbers);

        List<String> letters2 = new ArrayList<>(letterSet);
        recursiveRecursiveTree(numbers,letters2);

        System.out.println("=======  PUZZLE  =======");
        System.out.print("input: ");
        System.out.println(input1+" + "+input2+" = "+input3);
        System.out.print("unique letters: ");
        System.out.println(letterSet);
        System.out.print("total number combinations: ");
        System.out.println(combinationsCount);
        System.out.print("total solutions: ");
        System.out.println(solutionCount);
        System.out.print("example solution: ");
        System.out.println(solutionExample);
        System.out.println("=========================");
    }
    private static void recursiveTree(List<Integer> numbers){
        for (Integer number : numbers) {
            List<Integer> numbers2 = new ArrayList<>(numbers);
            numbers2.remove(number);
            numbersStack.push(number);
            if(numbersStack.size() == letterSet.size()) {
                //System.out.println(numbersStack); // res
                //System.out.println(lettersStack); // res
                checkForSolution();
            }
            recursiveTree(numbers2);
        }
        try {
            numbersStack.pop();
        } catch (Exception ignored){}
    }
    private static void checkForSolution(){
        combinationsCount++;
        HashMap<String, Integer> letterMap = new HashMap<>();
        StringBuilder input1NrString = new StringBuilder();
        StringBuilder input2NrString = new StringBuilder();
        StringBuilder input3NrString = new StringBuilder();
        StringBuilder solutionsString = new StringBuilder();

        GenericStack<Integer> numbersStack2 = (GenericStack<Integer>)numbersStack.clone();

        for (String letter : letterSet) {
            letterMap.put(letter, numbersStack2.pop());
        }
        for (String letter: input11) {
            input1NrString.append(letterMap.get(letter));
        }
        for (String letter: input22) {
            input2NrString.append(letterMap.get(letter));
        }
        for (String letter: input33) {
            input3NrString.append(letterMap.get(letter));
        }
        char c1 = input1NrString.charAt(0);
        char c2 = input2NrString.charAt(0);
        char c3 = input3NrString.charAt(0);
        if (c1 != '0' && c2 != '0' && c3 != '0') {
            long input1Long = Long.parseLong(input1NrString.toString());
            long input2Long = Long.parseLong(input2NrString.toString());
            long input3Long = Long.parseLong(input3NrString.toString());
            if (input1Long + input2Long == input3Long) {
                solutionCount++;
                solutionsString.append(input1Long).append(" + ").append(input2Long).append(" = ").append(input3Long);
                solutionExample = solutionsString.toString();
            }
        }

    }
   private static void recursiveRecursiveTree(List<Integer> numbers, List<String> letters){
      for (String letter : letters) {
         List<String> letters2 = new ArrayList<>(letters);
         letters2.remove(letter);
         for (Integer number : numbers) {
            List<Integer> numbers2 = new ArrayList<>(numbers);
            numbers2.remove(number);
            // System.out.println("---- next ----");
            numbersStack.push(number);
            lettersStack.push(letter);
            //System.out.println(numbers2);
            if (letters2.size() == 0) {
               //System.out.println("---- back (terminal) ----");
               //System.out.print("Stack: ");
               //System.out.print(numbersStack); // res
               if (numbersStack.size() == letterSet.size()) {
                  System.out.println(numbersStack); // res
                  System.out.println(lettersStack); // res
                  //testMap.put(numbersStack.toString(), null);
                  // TERMINAL OPERATION HERE //
               }
               try {
                  numbersStack.pop();
                  lettersStack.pop();
               } catch (Exception ignored) {
               }
            }
            recursiveRecursiveTree(numbers2, letters2);
         }
         //System.out.println("---- back ----");
         try {
            numbersStack.pop();
            lettersStack.pop();
         } catch (Exception ignored) {
         }
      }
   }
}
