import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class RandomizerTimer {
    Random rand = new Random();
    static int currRand;

    RandomizerTimer() {
        currRand = rand.nextInt(99);
        ActionListener actionListener = new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                currRand = rand.nextInt(99);
            }
        };
        Timer timer = new Timer(2000, actionListener);
        timer.start();
    }

    public static void main(String args[]) throws InterruptedException {
        RandomizerTimer te = new RandomizerTimer();
        while( true ) {
            Thread.currentThread().sleep(1000);
            System.out.println("current value:" + currRand );
        }
    }
}
