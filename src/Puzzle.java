
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Puzzle {

   /** Solve the word puzzle.
    * @param args three words (addend1 addend2 sum)
    */

   //private static GenericStack<Integer> numbersStack = new GenericStack<>();
   private static Deque<Integer> numbersStack = new ArrayDeque<>();
   private static int combinationsCount;
   private static int solutionCount;
   private static Set<String> letterSet = new LinkedHashSet<>();
   private static String solutionExample;
   private static String[] input11;
   private static String[] input22;
   private static String[] input33;

   public static void main(String[] args) {
      solvePuzzle("SEND","MORE","MONEY");
      solvePuzzle("YKS","KAKS","KOLM");
      solvePuzzle("ABCDEFGHIJAB","ABCDEFGHIJA","ACEHJBDFGIAC");
   }

   private static void solvePuzzle(String input1, String input2, String input3){
      numbersStack.clear();
      combinationsCount = 0;
      solutionCount = 0;
      letterSet.clear();
      solutionExample = null;
      input11= new String[0];
      input22= new String[0];
      input33= new String[0];
      long startTime = System.nanoTime();
      String inputCharsString = input1 + input2 + input3;
      String[] inputCharsArray = inputCharsString.split("");
      input11 = input1.split("");
      input22 = input2.split("");
      input33 = input3.split("");
      letterSet.addAll(Arrays.asList(inputCharsArray)); // get unique letters from input strings
      int set = 9; // 0-X numbers range
      List<Integer> numbers = IntStream.rangeClosed(0, set).boxed().collect(Collectors.toList());
      recursiveTree(numbers);
      long endTime = System.nanoTime();
      long duration = (endTime - startTime);  //divide by 1000000 to get milliseconds.
      long msecs = (duration / 1000000);
      System.out.println("===============  PUZZLE  ==============");
      System.out.print("Input: ");
      System.out.println(input1+" + "+input2+" = "+input3);
      System.out.print("Unique letters: ");
      System.out.println(letterSet);
      System.out.print("Total number of combinations: ");
      System.out.println(combinationsCount);
      System.out.print("Total solutions: ");
      System.out.println(solutionCount);
      System.out.print("Example solution: ");
      System.out.println(solutionExample);
      System.out.println("Calculate time: "+msecs+"ms");
      System.out.println("=======================================");
   }
   private static void recursiveTree(List<Integer> numbers){
      for (Integer number : numbers) {
         List<Integer> numbers2 = new ArrayList<>(numbers);
         numbers2.remove(number);
         numbersStack.push(number);
         if(numbersStack.size() == letterSet.size()) {
            checkForSolution();
         }
         recursiveTree(numbers2);
      }
      try {
         numbersStack.pop();
      } catch (Exception ignored){}
   }
   private static void checkForSolution(){
      combinationsCount++;
      HashMap<String, Integer> letterMap = new HashMap<>();
      StringBuilder input1NrString = new StringBuilder();
      StringBuilder input2NrString = new StringBuilder();
      StringBuilder input3NrString = new StringBuilder();
      StringBuilder solutionsString = new StringBuilder();
      Deque<Integer> numbersStack2 = new ArrayDeque<>(numbersStack);
      for (String letter : letterSet) {
         letterMap.put(letter, numbersStack2.pop());
      }
      for (String letter: input11) {
         input1NrString.append(letterMap.get(letter));
      }
      for (String letter: input22) {
         input2NrString.append(letterMap.get(letter));
      }
      for (String letter: input33) {
         input3NrString.append(letterMap.get(letter));
      }
      char c1 = input1NrString.charAt(0);
      char c2 = input2NrString.charAt(0);
      char c3 = input3NrString.charAt(0);
      if (c1 != '0' && c2 != '0' && c3 != '0') {
         long input1Long = Long.parseLong(input1NrString.toString());
         long input2Long = Long.parseLong(input2NrString.toString());
         long input3Long = Long.parseLong(input3NrString.toString());
         if (input1Long + input2Long == input3Long) {
            solutionCount++;
            solutionsString.append(input1Long).append(" + ").append(input2Long).append(" = ").append(input3Long);
            solutionExample = solutionsString.toString();
         }
      }
   }
}

