import java.util.LinkedList;
import java.util.List;


public class GenericStack<T> {

    private List<T> data;

    GenericStack() {
        this.data = new LinkedList<T>();
    }

    public List<T> getData() {
        return data;
    }

    @Override
    public Object clone() {
        GenericStack<T> c = new GenericStack<>();
        c.data.addAll(this.data);
        return c;
    }

    public boolean stEmpty() {
        if (data.size() == 0) return true;
        return false;
    }

    public Integer size() {
        return data.size();
    }

    public void push(T a) {
        data.add(data.size(), a);
    }

    public T pop() {
        if (data.size() <= 0) {
            throw new IndexOutOfBoundsException("No data entered!");
        } else {
            return data.remove(data.size() - 1);
        }

    } // pop

    public T tos() {
        if (data.size() <= 0) {
            throw new IndexOutOfBoundsException("Cannot retrieve top from empty stack!");
        } else {
            return data.get(data.size() - 1);
        }

    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o.getClass() != this.getClass()) return false;
        GenericStack oo = (GenericStack)o;
        return this.data.equals(oo.data);
        // return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (T el : data) {
            sb.append(el.toString()).append(" ");
        }
        return sb.toString();
    }
}
