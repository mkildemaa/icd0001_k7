import java.util.LinkedList;
import java.util.List;


public class IntegerStack {

    private List<Integer> data;

    public static void main(String[] argum) {
        IntegerStack mag = new IntegerStack();
    }

    IntegerStack() {
        this.data = new LinkedList<Integer>();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        IntegerStack c = new IntegerStack();
        c.data.addAll(this.data);
        return c;
    }

    public boolean stEmpty() {
        if (data.size() == 0) return true;
        return false;
    }

    public void push(Integer a) {
        data.add(data.size(), a);
    }

    public Integer pop() {
        if (data.size() <= 0) {
            throw new IndexOutOfBoundsException("No data entered!");
        } else {
            return data.remove(data.size() - 1);
        }

    } // pop

    public Integer tos() {
        if (data.size() <= 0) {
            throw new IndexOutOfBoundsException("Cannot retrieve top from empty stack!");
        } else {
            return data.get(data.size() - 1);
        }

    }

    public Integer size() {
        Integer i = 0;
        for (Integer ignored : data) {
            i++;
        }
        return i;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o.getClass() != this.getClass()) return false;
        IntegerStack oo = (IntegerStack) o;
        return this.data.equals(oo.data);
        // return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Integer integer : data) {
            sb.append(Integer.toString(integer)).append(" ");
        }
        return sb.toString();
    }
}
